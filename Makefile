.PHONY: all

all: tests/roles
	env ANSIBLE_HASH_BEHAVIOUR=merge \
	  ansible-playbook tests/test.yml --ask-become-pass \
	    --extra-vars ansible_python_interpreter=/usr/bin/python2

tests/roles: tests/requirements.yml
	ansible-galaxy install \
	  --roles-path=tests/roles --role-file=tests/requirements.yml
